### Hi there 👋

<!--
**Angel-noor/Angel-noor** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

<div align="center">
  <img src="https://github-profile-trophy.vercel.app/?username=Angel-noor&column=-1" alt="Angel-noor's GitHub trophy">
</div>

<hr>

[//]: https://github-readme-stats.vercel.app/api?username=Angel-noor&count_private=true&show_icons=true&theme=buefy&custom_title=🧶%20Angel%20is%20vibrating

<img align="right" src="https://github-readme-stats.vercel.app/api?username=Angel-noor&count_private=true&show_icons=true&theme=buefy&custom_title=🧶%20Angel%20is%20vibrating" alt="Angel-noor's GitHub Stats">

- 🐍 Python: junior
- 🦈 Docker: junior
- 📦 JavaScript: noob
- 👀 C/C++: junior
- 🎨 Graphic design: junior
- 🖼️ Image manipulation: junior
- 📷 Photography: junior
- 🎬 Non-linear editing: junior
- 📱 GUI: little expert
- 🍥 OS: testing Jelly
- 💭 Pronouns: she/her
- 💬 Telegram: [@devia_angel](https://t.me/devia_angel)
- 📣 Lang: `urdu`, `en`, `hindi`
